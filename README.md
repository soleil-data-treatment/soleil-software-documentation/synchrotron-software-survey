# Introduction

Dépôt git pour le tour des lignes commencé en Septembre 2020, réunissant la génération des fiches logicielles (code source dans le dossier projects). Les projets prennent en entrée les réponses du questionnaire en ligne (Google form).

Lien vers le formulaire en ligne: https://docs.google.com/forms/d/e/1FAIpQLSc8XYh54Pz2FZuN9T2bay7EyUWNiE2IQuUN-MCtmGCpgZDfbg/viewform

# Vue d'ensemble des fiches des logiciels

https://soleil-data-treatment.gitlab.io/soleil-software-documentation/synchrotron-software-survey
