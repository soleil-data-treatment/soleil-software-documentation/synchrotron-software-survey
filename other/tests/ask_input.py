import sys

import astropy
import astropy.table
import astropy.io.ascii

# return a file
def file_input(file_extension="", question="Entrez le fichier"): 
        while True:
            inp = input(question)
            file = ""
            try:
                file = open(inp, "r") # try to find the file
            except IOError:
                # if we get here user entered invalid input so print message and ask again
                print("Ce fichier n'existe pas")
                continue
            if file_extension != "":
                if inp.lower().endswith(file_extension):
                    return file
                else:
                    print("Ce fichier n'a pas la bonne extension")
                    continue
            else:
                return file

"""
with open('sample.md', 'r') as original_md:
    data = original_md.read()
print(original_md)
"""

print 'Number of arguments:', len(sys.argv), 'arguments.'
print 'Argument List:', str(sys.argv)

if len(sys.argv) != 1:
    exit

if inp.lower().endswith(file_extension)

csv_file_name = file_input(('.cvs'), "Entrez le nom du fichier que vous voulez lire")

data = astropy.io.ascii.read(csv_file_name, comment='#', delimiter=',')