# Liste TODO

fiches après 2021/01/05, site django formulaires, tutoriaux d'installation (sur windows ?)

| Logiciel | Fiche logicielle remplie avec liens vers les sources officielles | Fichiers téléchargeables et jeux de données | Tutoriaux et ressources disponibles | Fiche logicielle traduite en anglais | Code source et d'installation uploadés | Notes TODO |
| - | - | - | - | - | - | - |
| [ANA-ROD](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ana-rod-documentation) | ✅ | doc installation officielle |   |   |   | WIP tutoriel d'installation. Code source: email envoyé à l'auteur. Code d'installation: prendre un RV avec SIXS. Probablement apres le 22 janvier. |
| [BINoculars](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/binoculars-documentation) | ✅ | doc utilisateur, dataset SIXS | tutoriaux sur les sources officielles (uploadés sur gitlab) |   |   | WIP tutoriel d'installation |
| [Crysalis](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/crysalis-documentation) | ✅ | doc utilisateur, dataset pandata |   |   |   |   |
| [Fullprof](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/fullprof-documentation) | ✅ |   | tutoriaux sur les sources officielles |   |   | WIP tutoriel d'installation. Code source: email envoyé à l'auteur - Annual School on Neutron Diffraction Data Treatment using the FullProf Suite |
| [PyMca](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/pymca-documentation) | ✅ | doc officielle, dataset pandata | tutoriaux sur les sources officielles |   |   | WIP tutoriel d'installation |
| [Pynx](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/pynx-documentation) | ✅ |   | tutoriel d'installation sur wiki SOLEIL, tutoriaux sur les sources officielles |   |   |   |
| [XRSTools](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/xrstools-documentation) | ✅ |   |   |   |   |   |
| [Quanty](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/quanty-documentation) | ✅ |   | forum officiel |   |   | confluence vpn |
| [Matlab](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/matlab-documentation) | ✅ |   | tutoriaux sur les sources officielles |   |   |   |
| [Fiji - ImageJ](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/fiji-imagej-documentation) | ✅ |   | tutoriel d'installation et tutoriaux sur les sources officielles |   |   |   |
| [BeStSel](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/bestsel-documentation) | ✅ | doc officielle |   |   |   | Code source: à voir pour envoyer mail |
| [Quasar](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/quasar-documentation) | ✅ |   | tutoriaux sur les sources officielles |   |   |   |
| [Moulinex](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/moulinex-documentation) | WIP |   |   |   |   |   |
| [Imagine](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/imagine-documentation) | WIP |   |   |   |   |   |
| [Crispy](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/crispy-documentation) | ✅ |   | tutoriel d'installation et tutoriaux sur les sources officielles |   |   | confluence vpn |
| [Larch](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/larch-documentation) | ✅ |   | tutoriel d'installation et tutoriaux sur les sources officielles |   |   |   |
| [Demeter](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/demeter-documentation) | ✅ |   | tutoriel d'installation sur les sources officielles |   |   |   |
| [i2PEPICO](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/i2PEPICO-documentation) | ✅ |   |   |   |   |   |
| [VBasic-FTS](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/VBasic-FTS-documentation) | WIP |   |   |   |   |   |
| [SasView](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/SasView-documentation) | ✅ |   |   |   |   |   |
| [PyFAI](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/PyFAI-documentation) | ✅ |   |   |   |   |   |
| [silx](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/silx-documentation) | ✅ |   |   |   |   |   |
| [Igor Pro](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/Igor-Pro-documentation) | ✅ |   |   |   |   |   |
| [UFO](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/UFO-documentation) | ✅ |   |   |   |   |   |
