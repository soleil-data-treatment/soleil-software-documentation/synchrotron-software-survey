# Aide et ressources de Crysalis pour Synchrotron SOLEIL

[<img src="image url" width="250"/>](website)

## Résumé

- Réduction de données de diffraction. Reconstruction d'image. Permet l'analyse 
- Privé

## Sources

- Code source: Non, pas de distribution
- Documentation officielle: Oui

## Navigation rapide

| Wiki SOLEIL | Fichiers téléchargeables | Tutoriaux | Ressources annexes | Page pan-data |
| - | - | - | - | - |
| [Tutoriaux SOLEIL](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /wikis/home) | [Documents d'utilisation](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /-/tree/master/documents) | [Tutoriel d'installation officiel](lien) | [Guides d'utilisateurs](lien) | [Documentation pan-data du logiciel](https://software.pan-data.eu/software/) |
|   | [Jeux de données](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /-/tree/master/dataset) | [Tutoriaux officiels](lien) |   |   |

## Installation

- Systèmes d'exploitation supportés: Windows
- Installation: Facile (tout se passe bien)

## Format de données

- en entrée: Images propriétaires issues de leurs propres détecteurs ou autres formats (nombre restreint) (ex: esperanto)
- en sortie: Images reconstruites (.jpeg) analyse d'images (.txt)
- sur un disque dur, sur la Ruche, sur la Ruche locale
