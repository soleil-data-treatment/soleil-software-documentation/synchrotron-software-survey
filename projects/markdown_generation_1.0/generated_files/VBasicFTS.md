# Aide et ressources de VBasicFTS pour Synchrotron SOLEIL

[<img src="image url" width="250"/>](website)

## Résumé

- Acquisition et calcul des spectres.
- Créé à Synchrotron Soleil

## Sources

- Code source: Non, pas de distribution
- Documentation officielle: Oui

## Navigation rapide

| Wiki SOLEIL | Fichiers téléchargeables | Tutoriaux | Ressources annexes | Page pan-data |
| - | - | - | - | - |
| [Tutoriaux SOLEIL](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /wikis/home) | [Documents d'utilisation](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /-/tree/master/documents) | [Tutoriel d'installation officiel](lien) | [Guides d'utilisateurs](lien) | [Documentation pan-data du logiciel](https://software.pan-data.eu/software/) |
|   | [Jeux de données](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /-/tree/master/dataset) | [Tutoriaux officiels](lien) |   |   |

## Installation

- Systèmes d'exploitation supportés: Windows
- Installation: En général les utilisateurs exploitent les fichiers txt produits par VB

## Format de données

- en entrée: Les données sont enregistrées sous un format binaire
- en sortie: Les données sont exportées vers un simple fichier ascii
- sur un disque dur, sur la Ruche
