# Aide et ressources de i2PEPICO pour Synchrotron SOLEIL

[<img src="image url" width="250"/>](website)

## Résumé

- conversion puis extraction des vitesses des ions et des électrons
- Créé à Synchrotron Soleil

## Sources

- Code source: Oui, mais sur un dépôt interne a Soleil
- Documentation officielle: Oui

## Navigation rapide

| Wiki SOLEIL | Fichiers téléchargeables | Tutoriaux | Ressources annexes | Page pan-data |
| - | - | - | - | - |
| [Tutoriaux SOLEIL](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /wikis/home) | [Documents d'utilisation](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /-/tree/master/documents) | [Tutoriel d'installation officiel](lien) | [Guides d'utilisateurs](lien) | [Documentation pan-data du logiciel](https://software.pan-data.eu/software/) |
|   | [Jeux de données](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /-/tree/master/dataset) | [Tutoriaux officiels](lien) |   |   |

## Installation

- Systèmes d'exploitation supportés: Windows, MacOS
- Installation: Facile (tout se passe bien), installation igor nécessaire

## Format de données

- en entrée: binaire, séquence de mot 32 bits
- en sortie: fichier binaire, structure en c, fichiers texte, fichiers image, fichier igor
- sur un disque dur, sur la Ruche locale, git interne à SOLEIL
