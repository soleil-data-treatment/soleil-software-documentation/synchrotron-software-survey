# Aide et ressources de XRSTools pour Synchrotron SOLEIL

[<img src="image url" width="250"/>](website)

## Résumé

- Outils de réduction de données Raman X 
- Open source

## Sources

- Code source: https://gitlab.esrf.fr/mirone/XRStools
- Documentation officielle: Oui

## Navigation rapide

| Wiki SOLEIL | Fichiers téléchargeables | Tutoriaux | Ressources annexes | Page pan-data |
| - | - | - | - | - |
| [Tutoriaux SOLEIL](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /wikis/home) | [Documents d'utilisation](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /-/tree/master/documents) | [Tutoriel d'installation officiel](lien) | [Guides d'utilisateurs](lien) | [Documentation pan-data du logiciel](https://software.pan-data.eu/software/) |
|   | [Jeux de données](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /-/tree/master/dataset) | [Tutoriaux officiels](lien) |   |   |

## Installation

- Systèmes d'exploitation supportés: Windows, Linux, MacOS, Machine virtuelle
- Installation: Difficile (très technique)

## Format de données

- en entrée: hdf5, edf
- en sortie: h5
- sur la Ruche
