# Aide et ressources de SILX pour Synchrotron SOLEIL

[<img src="image url" width="250"/>](website)

## Résumé

- Exploration & visualisation des données dans les structures de fichier *.nxs ; export des images au format *.edf ; ajsutement de données par des solutions analytiques
- Je ne sais pas

## Sources

- Code source: Je ne sais pas
- Documentation officielle: Oui

## Navigation rapide

| Wiki SOLEIL | Fichiers téléchargeables | Tutoriaux | Ressources annexes | Page pan-data |
| - | - | - | - | - |
| [Tutoriaux SOLEIL](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /wikis/home) | [Documents d'utilisation](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /-/tree/master/documents) | [Tutoriel d'installation officiel](lien) | [Guides d'utilisateurs](lien) | [Documentation pan-data du logiciel](https://software.pan-data.eu/software/) |
|   | [Jeux de données](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /-/tree/master/dataset) | [Tutoriaux officiels](lien) |   |   |

## Installation

- Systèmes d'exploitation supportés: 
- Installation: Déployé dans le package géré par GRADES

## Format de données

- en entrée: fichier *.nxs généré par SOLEIL, sinon fichiers *.txt ou *.dat, images,...
- en sortie: images *.edf pour pyFAI sinon fichier de tableau de données *.txt ou *.dat
- sur la Ruche
