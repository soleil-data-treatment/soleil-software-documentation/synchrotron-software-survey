# Aide et ressources de SMIS python scripts pour Synchrotron SOLEIL

[<img src="image url" width="250"/>](website)

## Résumé

- data analysis, data reading, data collection
- Open source

## Sources

- Code source: https://github.com/smis-soleil
- Documentation officielle: Oui

## Navigation rapide

| Wiki SOLEIL | Fichiers téléchargeables | Tutoriaux | Ressources annexes | Page pan-data |
| - | - | - | - | - |
| [Tutoriaux SOLEIL](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /wikis/home) | [Documents d'utilisation](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /-/tree/master/documents) | [Tutoriel d'installation officiel](lien) | [Guides d'utilisateurs](lien) | [Documentation pan-data du logiciel](https://software.pan-data.eu/software/) |
|   | [Jeux de données](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /-/tree/master/dataset) | [Tutoriaux officiels](lien) |   |   |

## Installation

- Systèmes d'exploitation supportés: Windows, Linux, MacOS
- Installation: Facile (tout se passe bien)

## Format de données

- en entrée: commercial formats, not complex
- en sortie: csv, orange models
- sur la Ruche
