# Aide et ressources de SASVIEW pour Synchrotron SOLEIL

[<img src="image url" width="250"/>](website)

## Résumé

- Ajustement des données de diffusion des rayons X aux petits angles (SAXS) avec les différents modèles de facteur de structure proposé.
- Open source

## Sources

- Code source: Je ne sais pas
- Documentation officielle: Oui

## Navigation rapide

| Wiki SOLEIL | Fichiers téléchargeables | Tutoriaux | Ressources annexes | Page pan-data |
| - | - | - | - | - |
| [Tutoriaux SOLEIL](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /wikis/home) | [Documents d'utilisation](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /-/tree/master/documents) | [Tutoriel d'installation officiel](lien) | [Guides d'utilisateurs](lien) | [Documentation pan-data du logiciel](https://software.pan-data.eu/software/) |
|   | [Jeux de données](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /-/tree/master/dataset) | [Tutoriaux officiels](lien) |   |   |

## Installation

- Systèmes d'exploitation supportés: 
- Installation: gérer dans le package déployé par GRADES

## Format de données

- en entrée: donnée réduite par PyFAI,  fichier de type Intensité =(fQ)
- en sortie: tableau de donnée *.txt ou *.dat
- sur la Ruche
