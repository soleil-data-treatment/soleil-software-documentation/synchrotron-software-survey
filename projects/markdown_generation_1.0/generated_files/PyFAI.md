# Aide et ressources de PyFAI pour Synchrotron SOLEIL

[<img src="image url" width="250"/>](website)

## Résumé

- réduction des données des détecteurs images 2D
- Je ne sais pas

## Sources

- Code source: Je ne sais pas
- Documentation officielle: Oui

## Navigation rapide

| Wiki SOLEIL | Fichiers téléchargeables | Tutoriaux | Ressources annexes | Page pan-data |
| - | - | - | - | - |
| [Tutoriaux SOLEIL](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /wikis/home) | [Documents d'utilisation](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /-/tree/master/documents) | [Tutoriel d'installation officiel](lien) | [Guides d'utilisateurs](lien) | [Documentation pan-data du logiciel](https://software.pan-data.eu/software/) |
|   | [Jeux de données](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /-/tree/master/dataset) | [Tutoriaux officiels](lien) |   |   |

## Installation

- Systèmes d'exploitation supportés: 
- Installation: gérer dans la package déployé par GRADES

## Format de données

- en entrée: image *.edf
- en sortie: *.edf sinon image intégré sous for me de tableau de donnée I = f(Theta, Q) au *.txt ou *.dat
- sur la Ruche
