# Aide et ressources de DataReducer pour Synchrotron SOLEIL

[<img src="image url" width="250"/>](website)

## Résumé

- Sommation (et normalisation) des données issues de scans répétés.
- Créé à Synchrotron Soleil

## Sources

- Code source: Oui, sur un dépôt ouvert en ligne (type Git, SourceForge, ...)
- Documentation officielle: Fichier "install.txt"

## Navigation rapide

| Wiki SOLEIL | Fichiers téléchargeables | Tutoriaux | Ressources annexes | Page pan-data |
| - | - | - | - | - |
| [Tutoriaux SOLEIL](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /wikis/home) | [Documents d'utilisation](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /-/tree/master/documents) | [Tutoriel d'installation officiel](lien) | [Guides d'utilisateurs](lien) | [Documentation pan-data du logiciel](https://software.pan-data.eu/software/) |
|   | [Jeux de données](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /-/tree/master/dataset) | [Tutoriaux officiels](lien) |   |   |

## Installation

- Systèmes d'exploitation supportés: Windows
- Installation: Nécessite l'installation d'une version donnée de Java ou bien d'un java "standalone". Plutôt bien documenté dans "install.txt"

## Format de données

- en entrée: Fichiers nexus
- en sortie: Fichier nexus contenant les données sommées
- sur un disque dur, sur la Ruche, sur la Ruche locale
