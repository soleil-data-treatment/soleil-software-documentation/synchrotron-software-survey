# Aide et ressources de BeStSel pour Synchrotron SOLEIL

[<img src="image url" width="250"/>](website)

## Résumé

- Reconnaissance de repliements de protéines à partir de spectres de CD // SRCD
- Collab. SOLEIL, Univ. Budapest

## Sources

- Code source: Non, pas de distribution
- Documentation officielle: Oui

## Navigation rapide

| Wiki SOLEIL | Fichiers téléchargeables | Tutoriaux | Ressources annexes | Page pan-data |
| - | - | - | - | - |
| [Tutoriaux SOLEIL](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /wikis/home) | [Documents d'utilisation](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /-/tree/master/documents) | [Tutoriel d'installation officiel](lien) | [Guides d'utilisateurs](lien) | [Documentation pan-data du logiciel](https://software.pan-data.eu/software/) |
|   | [Jeux de données](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /-/tree/master/dataset) | [Tutoriaux officiels](lien) |   |   |

## Installation

- Systèmes d'exploitation supportés: Linux, Machine virtuelle
- Installation: Web utilisation sur un serveur

## Format de données

- en entrée: ASCII, 
- en sortie: Rapport d'analyse en pdf png, TXT
- serveur: http://bestsel.elte.hu/
