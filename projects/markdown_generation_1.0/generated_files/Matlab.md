# Aide et ressources de Matlab pour Synchrotron SOLEIL

[<img src="image url" width="250"/>](website)

## Résumé

- Imaging Processing Tb, Statistical, PLS Tb chimiometrie EigenVector (commercial)
- Privé

## Sources

- Code source: gitlab de l'INRA, privé
- Documentation officielle: Oui

## Navigation rapide

| Wiki SOLEIL | Fichiers téléchargeables | Tutoriaux | Ressources annexes | Page pan-data |
| - | - | - | - | - |
| [Tutoriaux SOLEIL](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /wikis/home) | [Documents d'utilisation](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /-/tree/master/documents) | [Tutoriel d'installation officiel](lien) | [Guides d'utilisateurs](lien) | [Documentation pan-data du logiciel](https://software.pan-data.eu/software/) |
|   | [Jeux de données](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /-/tree/master/dataset) | [Tutoriaux officiels](lien) |   |   |

## Installation

- Systèmes d'exploitation supportés: Windows, Linux, MacOS
- Installation: Post-Doc/CDD ne peut pas installer Matlab

## Format de données

- en entrée: NGV, SPC Horiba, GRAMS, TIFF, texte 
- en sortie: variable: MAT, TIFF, texte
- sur un disque dur, sur la Ruche, communication trop lente, pb d'identification/acces aux fichiers
