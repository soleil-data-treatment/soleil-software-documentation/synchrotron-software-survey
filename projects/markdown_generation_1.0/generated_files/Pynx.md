# Aide et ressources de Pynx pour Synchrotron SOLEIL

[<img src="image url" width="250"/>](website)

## Résumé

- Analyse les données de diffractions cohérentes
- Open source

## Sources

- Code source: sur l'esrf
- Documentation officielle: Oui

## Navigation rapide

| Wiki SOLEIL | Fichiers téléchargeables | Tutoriaux | Ressources annexes | Page pan-data |
| - | - | - | - | - |
| [Tutoriaux SOLEIL](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /wikis/home) | [Documents d'utilisation](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /-/tree/master/documents) | [Tutoriel d'installation officiel](lien) | [Guides d'utilisateurs](lien) | [Documentation pan-data du logiciel](https://software.pan-data.eu/software/) |
|   | [Jeux de données](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /-/tree/master/dataset) | [Tutoriaux officiels](lien) |   |   |

## Installation

- Systèmes d'exploitation supportés: Linux, MacOS, python
- Installation: Facile (tout se passe bien), Relativement facilement sur debian

## Format de données

- en entrée: .cxdi
- en sortie: .hdf5
- sur la Ruche locale
