# Aide et ressources de imagine pour Synchrotron SOLEIL

[<img src="image url" width="250"/>](website)

## Résumé

- traitement exafs / imagerie : extraction NXS+EDF->HDF5 avec alignement, normalisation, extraction EXAFS, TF
- Créé à Synchrotron Soleil

## Sources

- Code source: Oui, mais sur un dépôt 'privé' externe (accès restreint a des personnes identifiées)
- Documentation officielle: docstring dans le code

## Navigation rapide

| Wiki SOLEIL | Fichiers téléchargeables | Tutoriaux | Ressources annexes | Page pan-data |
| - | - | - | - | - |
| [Tutoriaux SOLEIL](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /wikis/home) | [Documents d'utilisation](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /-/tree/master/documents) | [Tutoriel d'installation officiel](lien) | [Guides d'utilisateurs](lien) | [Documentation pan-data du logiciel](https://software.pan-data.eu/software/) |
|   | [Jeux de données](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /-/tree/master/dataset) | [Tutoriaux officiels](lien) |   |   |

## Installation

- Systèmes d'exploitation supportés: Linux
- Installation: Facile (tout se passe bien)

## Format de données

- en entrée: NXS+EDF
- en sortie: HDF5, ascii
- sur la Ruche locale
