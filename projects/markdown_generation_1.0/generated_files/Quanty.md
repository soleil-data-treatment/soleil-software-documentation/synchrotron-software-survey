# Aide et ressources de Quanty pour Synchrotron SOLEIL

[<img src="image url" width="250"/>](website)

## Résumé

- segmentation, I/O, recalage, interface iPyWidgets, 
- Créé à Synchrotron Soleil

## Sources

- Code source: Oui, sur un dépôt ouvert en ligne (type Git, SourceForge, ...)
- Documentation officielle: Non

## Navigation rapide

| Wiki SOLEIL | Fichiers téléchargeables | Tutoriaux | Ressources annexes | Page pan-data |
| - | - | - | - | - |
| [Tutoriaux SOLEIL](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /wikis/home) | [Documents d'utilisation](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /-/tree/master/documents) | [Tutoriel d'installation officiel](lien) | [Guides d'utilisateurs](lien) | [Documentation pan-data du logiciel](https://software.pan-data.eu/software/) |
|   | [Jeux de données](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /-/tree/master/dataset) | [Tutoriaux officiels](lien) |   |   |

## Installation

- Systèmes d'exploitation supportés: 
- Installation: Difficile (très technique)

## Format de données

- en entrée: TIFF, JSON, 
- en sortie: HDF5
- Copie locale, puis sur la Ruche a la fin
