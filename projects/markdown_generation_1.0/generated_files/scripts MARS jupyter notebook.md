# Aide et ressources de scripts MARS jupyter notebook pour Synchrotron SOLEIL

[<img src="image url" width="250"/>](website)

## Résumé

- réduction des données
- Créé à Synchrotron Soleil

## Sources

- Code source: Je ne sais pas
- Documentation officielle: les scripts sont commentés directement dans le notebook

## Navigation rapide

| Wiki SOLEIL | Fichiers téléchargeables | Tutoriaux | Ressources annexes | Page pan-data |
| - | - | - | - | - |
| [Tutoriaux SOLEIL](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /wikis/home) | [Documents d'utilisation](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /-/tree/master/documents) | [Tutoriel d'installation officiel](lien) | [Guides d'utilisateurs](lien) | [Documentation pan-data du logiciel](https://software.pan-data.eu/software/) |
|   | [Jeux de données](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /-/tree/master/dataset) | [Tutoriaux officiels](lien) |   |   |

## Installation

- Systèmes d'exploitation supportés: python
- Installation: Packages déployés par GRADES

## Format de données

- en entrée: *.nxs
- en sortie: *.txt ,  *.dat
- sur un disque dur, sur la Ruche
