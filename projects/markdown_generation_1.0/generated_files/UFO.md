# Aide et ressources de UFO pour Synchrotron SOLEIL

[<img src="image url" width="250"/>](website)

## Résumé

- Pour la tomographie : Pre-traitement, reconstruction et post-traitement
- Open source

## Sources

- Code source: Oui, sur un dépôt ouvert en ligne (type Git, SourceForge, ...)
- Documentation officielle: Oui

## Navigation rapide

| Wiki SOLEIL | Fichiers téléchargeables | Tutoriaux | Ressources annexes | Page pan-data |
| - | - | - | - | - |
| [Tutoriaux SOLEIL](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /wikis/home) | [Documents d'utilisation](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /-/tree/master/documents) | [Tutoriel d'installation officiel](lien) | [Guides d'utilisateurs](lien) | [Documentation pan-data du logiciel](https://software.pan-data.eu/software/) |
|   | [Jeux de données](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /-/tree/master/dataset) | [Tutoriaux officiels](lien) |   |   |

## Installation

- Systèmes d'exploitation supportés: Linux, MacOS, Necessite une pile OpenCL sur la machine
- Installation: Facile (tout se passe bien), Paquet debian existant, pour MacOS compilation (avec complication pour la partie OpenCL)

## Format de données

- en entrée: raw (binaire sans entête), hdf5, tiff (et autres formats images /classiques/)
- en sortie: raw, hdf5, pile tiff
- sur un disque dur
