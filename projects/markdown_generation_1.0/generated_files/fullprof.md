# Aide et ressources de fullprof pour Synchrotron SOLEIL

[<img src="image url" width="250"/>](website)

## Résumé

-  Suite logicielle. Sert à l'affinement de poudres. Analyse complète
- Privé

## Sources

- Code source: https://www.ill.eu/sites/fullprof/
- Documentation officielle: Oui

## Navigation rapide

| Wiki SOLEIL | Fichiers téléchargeables | Tutoriaux | Ressources annexes | Page pan-data |
| - | - | - | - | - |
| [Tutoriaux SOLEIL](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /wikis/home) | [Documents d'utilisation](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /-/tree/master/documents) | [Tutoriel d'installation officiel](lien) | [Guides d'utilisateurs](lien) | [Documentation pan-data du logiciel](https://software.pan-data.eu/software/) |
|   | [Jeux de données](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /-/tree/master/dataset) | [Tutoriaux officiels](lien) |   |   |

## Installation

- Systèmes d'exploitation supportés: Windows, Linux, MacOS
- Installation: Facile (tout se passe bien), Soucis droits administrateurs

## Format de données

- en entrée: diffractogrammes 1D, formats propriétaires, vaste variété de formats
- en sortie: coordonnées (.cif)
- sur un disque dur, sur la Ruche, clef usb
