# Aide et ressources de Scripts GALAXIES pour Synchrotron SOLEIL

[<img src="image url" width="250"/>](website)

## Résumé

- Acquisition pour les instruments, photo émission, traitement et pré traitement des données, analyse des images, convertir images en donnée exploitable, calcul nombre de Bragg, faire fonctionner les instruments
- Créé à Synchrotron Soleil

## Sources

- Code source: Oui, sur un dépôt ouvert en ligne (type Git, SourceForge, ...)
- Documentation officielle: Oui

## Navigation rapide

| Wiki SOLEIL | Fichiers téléchargeables | Tutoriaux | Ressources annexes | Page pan-data |
| - | - | - | - | - |
| [Tutoriaux SOLEIL](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /wikis/home) | [Documents d'utilisation](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /-/tree/master/documents) | [Tutoriel d'installation officiel](lien) | [Guides d'utilisateurs](lien) | [Documentation pan-data du logiciel](https://software.pan-data.eu/software/) |
|   | [Jeux de données](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /-/tree/master/dataset) | [Tutoriaux officiels](lien) |   |   |

## Installation

- Systèmes d'exploitation supportés: Windows, Linux, MacOS, Machine virtuelle
- Installation: Facile (tout se passe bien)

## Format de données

- en entrée: nexus
- en sortie: ascii, nexus
- sur un disque dur, sur la Ruche
