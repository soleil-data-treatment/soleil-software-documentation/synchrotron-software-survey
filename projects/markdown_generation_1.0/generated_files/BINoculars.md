# Aide et ressources de BINoculars pour Synchrotron SOLEIL

[<img src="image url" width="250"/>](website)

## Résumé

- réduction de données, transformation d'espace, visualisation 2D, intégration des F² le long des rod. 
- Open source

## Sources

- Code source: https://github.com/picca/binoculars
- Documentation officielle: Documentation obsolete et minimale.

## Navigation rapide

| Wiki SOLEIL | Fichiers téléchargeables | Tutoriaux | Ressources annexes | Page pan-data |
| - | - | - | - | - |
| [Tutoriaux SOLEIL](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /wikis/home) | [Documents d'utilisation](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /-/tree/master/documents) | [Tutoriel d'installation officiel](lien) | [Guides d'utilisateurs](lien) | [Documentation pan-data du logiciel](https://software.pan-data.eu/software/) |
|   | [Jeux de données](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /-/tree/master/dataset) | [Tutoriaux officiels](lien) |   |   |

## Installation

- Systèmes d'exploitation supportés: Linux, Machine virtuelle VirtualBox
- Installation: Facile (tout se passe bien), OK sur Debian, mais pas autre. Traitement à distance souhaitable.

## Format de données

- en entrée: NXS (HDF5)
- en sortie: HDF5
- Ruche locale
