# Aide et ressources de DEMETER pour Synchrotron SOLEIL

[<img src="image url" width="250"/>](website)

## Résumé

- Ce package regroupe un ensemble d'outils utilisé pour l'intégralité des données d'absorption des rayons x. Athéna permet de normaliser et préparer la données brute, Artémis permet de faire des fits de spectre EXAFS (signal utilisé en abs x pour retrouver les structures autour de l'atome excité pour les rayons x) 
- Open source

## Sources

- Code source: https://github.com/bruceravel/demeter
- Documentation officielle: Oui

## Navigation rapide

| Wiki SOLEIL | Fichiers téléchargeables | Tutoriaux | Ressources annexes | Page pan-data |
| - | - | - | - | - |
| [Tutoriaux SOLEIL](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /wikis/home) | [Documents d'utilisation](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /-/tree/master/documents) | [Tutoriel d'installation officiel](lien) | [Guides d'utilisateurs](lien) | [Documentation pan-data du logiciel](https://software.pan-data.eu/software/) |
|   | [Jeux de données](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /-/tree/master/dataset) | [Tutoriaux officiels](lien) |   |   |

## Installation

- Systèmes d'exploitation supportés: Windows, MacOS
- Installation: Facile (tout se passe bien)

## Format de données

- en entrée: Il faut des fichiers texte au départ. Une fois un projet sauvegardé il peut être réouvert avec des préférences sauvés par le logiciel.
- en sortie: On récupère des fichiers texte ou images.
- sur un disque dur
