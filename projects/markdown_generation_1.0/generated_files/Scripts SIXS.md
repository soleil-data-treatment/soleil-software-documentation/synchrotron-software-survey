# Aide et ressources de Scripts SIXS pour Synchrotron SOLEIL

[<img src="image url" width="250"/>](website)

## Résumé

- Outils développées par les scientifiques et utilisateurs. Visu rapide, ROI, correction d'intensité. XPADplot, XRDviewer ...
- Fait maison

## Sources

- Code source: XRDviewer deja sur Gitlab
- Documentation officielle: Non, sauf XRDviewer

## Navigation rapide

| Wiki SOLEIL | Fichiers téléchargeables | Tutoriaux | Ressources annexes | Page pan-data |
| - | - | - | - | - |
| [Tutoriaux SOLEIL](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /wikis/home) | [Documents d'utilisation](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /-/tree/master/documents) | [Tutoriel d'installation officiel](lien) | [Guides d'utilisateurs](lien) | [Documentation pan-data du logiciel](https://software.pan-data.eu/software/) |
|   | [Jeux de données](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /-/tree/master/dataset) | [Tutoriaux officiels](lien) |   |   |

## Installation

- Systèmes d'exploitation supportés: Windows, Linux, MacOS
- Installation: Facile (tout se passe bien), simple

## Format de données

- en entrée: NXS, variable...
- en sortie: variable
- sur un disque dur, Ruche locale
