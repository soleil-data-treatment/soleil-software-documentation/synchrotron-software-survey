# Aide et ressources de Moulinex pour Synchrotron SOLEIL

[<img src="image url" width="250"/>](website)

## Résumé

- extraction quick exafs vers du TXT
- Créé à Synchrotron Soleil

## Sources

- Code source: Non, pas de distribution
- Documentation officielle: Oui

## Navigation rapide

| Wiki SOLEIL | Fichiers téléchargeables | Tutoriaux | Ressources annexes | Page pan-data |
| - | - | - | - | - |
| [Tutoriaux SOLEIL](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /wikis/home) | [Documents d'utilisation](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /-/tree/master/documents) | [Tutoriel d'installation officiel](lien) | [Guides d'utilisateurs](lien) | [Documentation pan-data du logiciel](https://software.pan-data.eu/software/) |
|   | [Jeux de données](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /-/tree/master/dataset) | [Tutoriaux officiels](lien) |   |   |

## Installation

- Systèmes d'exploitation supportés: Linux
- Installation: Facile (tout se passe bien), dep. Qt4 pour la GUI

## Format de données

- en entrée: NXS
- en sortie: TXT
- sur la Ruche locale
