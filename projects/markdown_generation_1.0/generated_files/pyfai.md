# Aide et ressources de pyfai pour Synchrotron SOLEIL

[<img src="image url" width="250"/>](website)

## Résumé

- réduction de donnés de diff, integration radiale, microfaisceau balayé
- Open source

## Sources

- Code source: gitlab esrf et https://github.com/silx-kit/pyFAI
- Documentation officielle: Oui

## Navigation rapide

| Wiki SOLEIL | Fichiers téléchargeables | Tutoriaux | Ressources annexes | Page pan-data |
| - | - | - | - | - |
| [Tutoriaux SOLEIL](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /wikis/home) | [Documents d'utilisation](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /-/tree/master/documents) | [Tutoriel d'installation officiel](lien) | [Guides d'utilisateurs](lien) | [Documentation pan-data du logiciel](https://software.pan-data.eu/software/) |
|   | [Jeux de données](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /-/tree/master/dataset) | [Tutoriaux officiels](lien) |   |   |

## Installation

- Systèmes d'exploitation supportés: Windows, Linux, MacOS, python
- Installation: Facile (tout se passe bien), anaconda ou Deb

## Format de données

- en entrée: nexus, 100-1000 images
- en sortie: ASCII, EDF, un fichier par pixel image
- sur la Ruche locale
