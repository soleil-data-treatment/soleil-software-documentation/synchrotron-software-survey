# Aide et ressources de Binoculars pour Synchrotron SOLEIL

[<img src="image url" width="250"/>](website)

## Résumé

- Visualisation et réduction de données dans l'espace réciproque
- Open source

## Sources

- Code source: Oui, sur un dépôt ouvert en ligne (type Git, SourceForge, ...)
- Documentation officielle: Oui

## Navigation rapide

| Wiki SOLEIL | Fichiers téléchargeables | Tutoriaux | Ressources annexes | Page pan-data |
| - | - | - | - | - |
| [Tutoriaux SOLEIL](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /wikis/home) | [Documents d'utilisation](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /-/tree/master/documents) | [Tutoriel d'installation officiel](lien) | [Guides d'utilisateurs](lien) | [Documentation pan-data du logiciel](https://software.pan-data.eu/software/) |
|   | [Jeux de données](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /-/tree/master/dataset) | [Tutoriaux officiels](lien) |   |   |

## Installation

- Systèmes d'exploitation supportés: Linux
- Installation: Facile (tout se passe bien)

## Format de données

- en entrée: fichiers nexus
- en sortie: 1 fichier hdf5
- sur la Ruche, sur la Ruche locale, sur la ligne Cristal
