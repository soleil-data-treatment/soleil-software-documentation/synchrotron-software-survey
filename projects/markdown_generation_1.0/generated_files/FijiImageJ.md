# Aide et ressources de FijiImageJ pour Synchrotron SOLEIL

[<img src="image url" width="250"/>](website)

## Résumé

- Développer des scripts / workflows d'analyse d'images, de stacks, de réduction de données.
- Open source

## Sources

- Code source: Oui, sur un dépôt ouvert en ligne (type Git, SourceForge, ...)
- Documentation officielle: Oui

## Navigation rapide

| Wiki SOLEIL | Fichiers téléchargeables | Tutoriaux | Ressources annexes | Page pan-data |
| - | - | - | - | - |
| [Tutoriaux SOLEIL](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /wikis/home) | [Documents d'utilisation](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /-/tree/master/documents) | [Tutoriel d'installation officiel](lien) | [Guides d'utilisateurs](lien) | [Documentation pan-data du logiciel](https://software.pan-data.eu/software/) |
|   | [Jeux de données](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /-/tree/master/dataset) | [Tutoriaux officiels](lien) |   |   |

## Installation

- Systèmes d'exploitation supportés: Windows, Linux, MacOS, Machine virtuelle
- Installation: Facile (tout se passe bien)

## Format de données

- en entrée: TIFF, collection d'images dans des sous dossiers, Bio-Format, hdf5, ...
- en sortie: Idem, le plus souvent collection d'images dans des sous dossiers.
- sur un disque dur
