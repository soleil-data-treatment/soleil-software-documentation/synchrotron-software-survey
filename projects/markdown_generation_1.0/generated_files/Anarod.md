# Aide et ressources de Anarod pour Synchrotron SOLEIL

[<img src="image url" width="250"/>](website)

## Résumé

- ANA: Integration des rod F². ROD: fit du F² le long des rods et comparaison avec un modèle.
- Open source, http://www.esrf.eu/computing/scientific/joint_projects/ANA-ROD/

## Sources

- Code source: Probablement gitlab.esrf
- Documentation officielle: Oui

## Navigation rapide

| Wiki SOLEIL | Fichiers téléchargeables | Tutoriaux | Ressources annexes | Page pan-data |
| - | - | - | - | - |
| [Tutoriaux SOLEIL](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /wikis/home) | [Documents d'utilisation](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /-/tree/master/documents) | [Tutoriel d'installation officiel](lien) | [Guides d'utilisateurs](lien) | [Documentation pan-data du logiciel](https://software.pan-data.eu/software/) |
|   | [Jeux de données](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /-/tree/master/dataset) | [Tutoriaux officiels](lien) |   |   |

## Installation

- Systèmes d'exploitation supportés: Windows, Linux
- Installation: Facile (tout se passe bien)

## Format de données

- en entrée: texte
- en sortie: texte, PGPLOT
- sur un disque dur, Ruche locale
