# Aide et ressources de LARCH pour Synchrotron SOLEIL

[<img src="image url" width="250"/>](website)

## Résumé

- Logiciel basé sur python pour le traitement des données XAS
- Open source

## Sources

- Code source: https://github.com/xraypy/xraylarch
- Documentation officielle: Oui

## Navigation rapide

| Wiki SOLEIL | Fichiers téléchargeables | Tutoriaux | Ressources annexes | Page pan-data |
| - | - | - | - | - |
| [Tutoriaux SOLEIL](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /wikis/home) | [Documents d'utilisation](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /-/tree/master/documents) | [Tutoriel d'installation officiel](lien) | [Guides d'utilisateurs](lien) | [Documentation pan-data du logiciel](https://software.pan-data.eu/software/) |
|   | [Jeux de données](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /-/tree/master/dataset) | [Tutoriaux officiels](lien) |   |   |

## Installation

- Systèmes d'exploitation supportés: Linux, MacOS
- Installation: Facile (tout se passe bien)

## Format de données

- en entrée: n'importe quel type de fichier importable en python
- en sortie: tout type d'export python
- sur un disque dur, sur la Ruche
