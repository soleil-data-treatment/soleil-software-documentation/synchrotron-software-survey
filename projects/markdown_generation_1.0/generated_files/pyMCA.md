# Aide et ressources de pyMCA pour Synchrotron SOLEIL

[<img src="image url" width="250"/>](website)

## Résumé

- visualisation et traitement de données
- Je ne sais pas

## Sources

- Code source: Je ne sais pas
- Documentation officielle: Je ne sais pas

## Navigation rapide

| Wiki SOLEIL | Fichiers téléchargeables | Tutoriaux | Ressources annexes | Page pan-data |
| - | - | - | - | - |
| [Tutoriaux SOLEIL](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /wikis/home) | [Documents d'utilisation](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /-/tree/master/documents) | [Tutoriel d'installation officiel](lien) | [Guides d'utilisateurs](lien) | [Documentation pan-data du logiciel](https://software.pan-data.eu/software/) |
|   | [Jeux de données](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/ [nom git] /-/tree/master/dataset) | [Tutoriaux officiels](lien) |   |   |

## Installation

- Systèmes d'exploitation supportés: Windows, Linux, MacOS
- Installation: Facile (tout se passe bien)

## Format de données

- en entrée: TXT, NEXUS
- en sortie: plusieurs types
- sur un disque dur, sur la Ruche
