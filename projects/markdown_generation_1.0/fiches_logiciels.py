import sys, getopt, os.path, shutil, re

import math

import string

import pandas as pd

# return a string
def ask_for_valid_string(question, max_length=20):

    invalid_characters= set(string.punctuation)
    print(question, "." , "Il ne doit pas contenir, ",invalid_characters)
    while (1):
        answer = input(" entrez votre string: ")
        if any(char in invalid_characters for char in answer) or len(answer)>max_length:
            pass
        else:
            return answer

# return a boolean
def ask_yes_no_question(question):
    yes_no_answer = input(question + " répondez par yes [y] ou no [n]: ")

    if yes_no_answer == "y":
        return True
    else:
        return False

def main(argv):

    input_file = ""

    try:
        opts, args = getopt.getopt(argv,"hi:o:",["ifile=","ofile="])
    except getopt.GetoptError:
        print ("test.py <input file.csv>")
        exit(1)

    for opt in opts:
        if opt == "-h":
            print ("test.py <input file>")
            exit()

    if len(args) != 1:
        print ("test.py <input file>")
        exit()

    input_file = args[0]
    
    if not input_file.lower().endswith(".csv"):
        print ("input file needs to be .csv")
        exit()

    data_frame = pd.read_csv(input_file)

    sample_file_data = "" 

    try:
        with open("sample.md", "r") as file : # try to find the file
            sample_file_data = file.read()
    except IOError:
        # if we get here user entered invalid input so print message and ask again
        print("file not found")
        exit(1)

    data_frame.columns = range(len(data_frame.columns))

    folder_path = "./generated_files"
    try:
        os.path.isdir(folder_path)
    except IOError:
        print("Folder not accessible")

    if ask_yes_no_question("Le contenu du dossier generated_file va être supprimé") is True:
        for root, dirs, files in os.walk(folder_path):
            for file in files:
                if file != "generated files are here":
                    os.remove(os.path.join(root, file))
    else:
        exit



    if ask_yes_no_question("Voulez vous créer une fiche logicielle pour" + " " + " } { ".join(data_frame[3])) is True:

        for software_name in data_frame[3]: # boucle à travers les logiciels

            # print(software_name) # nom logiciel

            markdown_skeleton = sample_file_data

            row_number = data_frame[data_frame[3] == str(software_name)].index[0]

            array_software_type = data_frame[9][row_number].split(";") # open source ou non
            array_code_link = data_frame[11][row_number].split(";") # lien code source
            array_documentation_link = data_frame[23][row_number].split(";") # lien documentation officielle

            array_os = data_frame[13][row_number].split(";")
            array_difficulty = data_frame[12][row_number].split(";")

            for os_name in array_os: # on affiche que Windows Linux MacOS ou Machine virtuelle
                if os_name == "Windows" or os_name == "Linux" or os_name == "MacOS" or os_name == "Machine virtuelle":
                    pass
                else:
                    array_os.remove(os_name)

            # array_input array_output
            array_input = ""
            try:
                array_input = data_frame[6][row_number].split(";")
            except AttributeError:
                array_input = [" "]

            array_output= ""
            try:
                array_output = data_frame[7][row_number].split(";")
            except AttributeError:
                array_output = [" "]

            try: 
                array_saved_on = data_frame[8][row_number].split(";")
            except AttributeError:
                print(software_name, array_saved_on)

            invalid_characters= set(string.punctuation)
            if any(char in invalid_characters for char in software_name) or len(software_name)>30:
                print ("Ce logiciel a un nom invalide"+" "+"{ "+ software_name +" }")
                software_name = ask_for_valid_string("Le nom ne doit pas contenir de virgule ou est trop long, veuillez entre un nom plus simple")

            markdown_skeleton = re.sub("software_name", software_name, markdown_skeleton, 1)
            # ---
            markdown_skeleton = re.sub("software_description", data_frame[4][row_number], markdown_skeleton, 1)
            markdown_skeleton = re.sub("array_software_type", ", ".join(array_software_type), markdown_skeleton, 1)
            # ---
            markdown_skeleton = re.sub("array_code_link", str(array_code_link[-1]), markdown_skeleton, 1)
            markdown_skeleton = re.sub("array_documentation_link", str(array_documentation_link[-1]), markdown_skeleton, 1)
            # ---
            markdown_skeleton = re.sub("array_os", ", ".join(array_os), markdown_skeleton, 1)
            markdown_skeleton = re.sub("array_difficulty", ", ".join(array_difficulty), markdown_skeleton, 1)
            # ---
            markdown_skeleton = re.sub("array_input", ", ".join(array_input), markdown_skeleton, 1)
            markdown_skeleton = re.sub("array_output", ", ".join(array_output), markdown_skeleton, 1)
            markdown_skeleton = re.sub("saved_on", ", ".join(array_saved_on), markdown_skeleton, 1)
                    
            # print(markdown_skeleton) # le texte qu'on met dans le fichier et qui a été modifié
                    
            with open(str(software_name) + ".md", "w+") as file:
                file.write(markdown_skeleton) # create new file per software

            shutil.move(str(software_name) + ".md", "./generated_files/" + str(software_name) + ".md")

if __name__ == "__main__":
   main(sys.argv[1:])