## Explication des projets

### Projet /data_analysis
- Analyse de données du formulaire avec notebook jupyter.

### Projet /markdown_generation_1.0
- Première version d'un projet pour générer des documentations en markdown à partir des réponses du formulaire. Projet fait en python.

### Projet /markdown_generation_2.0
- Deuxième version d'un projet pour générer des documentations en markdown à partir des réponses du formulaire.
Le but était de récupérer les réponses d'un Google Form questionnant les scientifiques des lignes de lumières sur l'utilisation de leurs logiciels. On peut afficher les réponses du formulaire de manière organisée, mais aussi créer automatiquement des dépôts Gitlab comprenant de la documentation markdown pour chaque logiciel. Projet MVC fait en Django, Bootstrap, communiquant avec l'API Google et Gitlab.
