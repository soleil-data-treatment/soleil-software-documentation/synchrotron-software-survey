# google apis
import gspread
from google.oauth2 import service_account
from google.auth.transport.requests import AuthorizedSession

# other librairies
import json
import os
import re
from datetime import datetime
import time

cwd = os.getcwd()

# static path
tokens_path = cwd + "/controllers/tokens/"
json_path = cwd + "/controllers/json/"
markdown_path = cwd + "/controllers/markdown/"

# ---- google sheets functions

def refine_data_before_use(answers):
    """refining the data before using it, in order to avoid bad cases

    Args:
        answers (list of dicts): list of answers to refine

    Returns:
        list of dicts: answers
    """     
    
    # handle duplicates
    for answer in answers:
        # all beamlines are upper case
        answer["Sur quelle ligne de lumière êtes-vous ?"] = answer["Sur quelle ligne de lumière êtes-vous ?"].upper()

    return answers


def save_answers_as_file(answers):
    """save in the static/json folder the answers so that even if the google sheets is deleted
    the django website will still be working properly

    Args:
        answers (list of dicts): list of answers
    """

    with open(json_path + 'answers.json', "w") as json_answers: # save answers in the file
                json_answers.write(json.dumps(answers, indent=4))
          

def get_form_answers():
    """retrieve google form answers from google sheet (raw data)

    Returns:
        list of dicts: answers
    """

    answers = [{"toto": 1, "titi": 2}] # init

    try:
        f = open(json_path + 'answers.json') # check if file exists
        # do something with the file
    except IOError:
        print("file not accessible")
        f = open(json_path + 'answers.json', "w") # create file
        f.write(json.dumps(answers, indent=4))
    finally:
        f.close()

    try:
        credentials = service_account.Credentials.from_service_account_file(
            tokens_path + 'soleil-software-documentation-6ec8610a9426.json')

        scoped_credentials = credentials.with_scopes(
            ['https://spreadsheets.google.com/feeds', 'https://www.googleapis.com/auth/drive'])

        gc = gspread.Client(auth=scoped_credentials)
        gc.session = AuthorizedSession(scoped_credentials)
        # key in the url when using share on google sheets
        ACCESS_TOKEN = ''
        with open(tokens_path + 'google_sheets.json') as json_token: # access google sheets with access token
            data = json.load(json_token)
            ACCESS_TOKEN = data['ACCESS_TOKEN']
        sheet = gc.open_by_key(ACCESS_TOKEN)

        answers = sheet.sheet1.get_all_records() # get all records
        save_answers_as_file(answers)
        
    except:
        print("cannot get the google sheets with the access token")
        with open(json_path + 'answers.json') as json_answers: # read answers from the file
            answers = json.load(json_answers)

    answers = refine_data_before_use(answers)

    return answers  # python dictionary

# ---- auxiliary functions

def refine_answers_for_softwares_sheets(answers):
    """
    refine the answers in order to display them in the view softwares_sheets
    loop through the software json list and get infos of this software from the form answers

    Args:
        answers (list of dicts): list of answers to refine

    Returns:
        list of dicts: less detailed list of answers
    """    

    result = []

    with open(json_path + 'git-list.json') as git_list_json:
        git_list_data = json.load(git_list_json)

        # loop through the known git name lists
        for item in git_list_data:

            # row for results, one row = software (name + form infos)
            result_row = {'software_infos': item, 'beamlines_data': []}

            # search informations about this software through the answers from the google form asnwers
            for answer in answers:
                # we find if this answer has the name of a known software
                string = answer["Donnez le nom du logiciel que vous utilisez : "].lower(
                )
                substring = item["software name"].lower()

                # looking if this software is in our list
                occurence_result = re.findall(substring, string)
                if len(occurence_result) == 0:
                    pass
                else:
                    beamlines_data_row = {
                        # basic informations
                        'beamline name': answer["Sur quelle ligne de lumière êtes-vous ?"],
                        'description': answer["Expliquez brièvement à quoi sert ce logiciel :"],
                        # general informations
                        'importance': answer["Comment estimez-vous l'importance de ce logiciel ?"],
                        'installation': answer["Comment se passe l'installation du logiciel ?"],
                        'os support': answer["Sur quoi peut-on utiliser le logiciel ? Si vous le savez, quel type de packaging est utilisé ?"],
                        # data informations
                        'input data': answer["Quels sont les formats de données de l'on donne au logiciel ? (input)"],
                        'output data': answer["Quels formats peut-on récupérer ? (output)"],
                        'data location': answer["Où sont stockées les données traitées\xa0?"],
                        # development informations
                        'development status': answer["Comment se passe le développement du logiciel ?"],
                        'number people': answer["Combien de personnes s'occupent du logiciel ?"],
                        'first needed features': answer["Quelles seraient les fonctionnalités vitales à ajouter ou corriger en premier lieu pour améliorer le logiciel ?"],
                        'secondary needed features': answer["Quelles seraient les fonctionnalités secondaires à ajouter dans le futur pour améliorer le logiciel ?"],
                        # documentation informations
                        'learning time': answer["Combien de temps estimez-vous consacrer en moyenne pour apprendre à utiliser ce logiciel ?"],
                        'tutorials': answer["Pensez-vous que créer des tutoriels en ligne (au format vidéo ou non) portant sur les cas d'usage les plus fréquents du logiciel, afin de guider les utilisateurs serait envisageable ? Si non, pourquoi ?"],
                        'data sets': answer["Y a t-il des jeux de données pour tester/former les utilisateurs ?"],
                    }

                    result_row["beamlines_data"].append(beamlines_data_row)

            result.append(result_row)

        git_list_json.close()

    return result


def is_this_software_exists(software):
    """with an input software string, search it in our json list in order to known if it exists

    Args:
        software (string): a software

    Returns:
        Boolean: True: this software exists or not: False
    """    

    string = software.lower()  # what we want to search for

    with open(json_path + 'git-list.json') as git_list_json:
        git_list_data = json.load(git_list_json)

        # search through the known git name lists
        for item in git_list_data:

            substring = item["software name"].lower()
            # looking if this software is in our list
            occurence_result = re.findall(substring, string)
            if len(occurence_result) == 0:
                pass
            else:
                return True

    return False


def can_be_added_softwares(answers, MAX_LENGTH=40):
    """refine the data by adding an attribute to know if each software can be added as a new software

    Args:
        answers (list of dicts): list of answers
        MAX_LENGTH (int, optional): maximum lenght for a software's name. Defaults to 40.

    Returns:
        list of dicts: list of answers with new attributes: can be added True/False
    """    

    for answer in answers:

        string = answer["Donnez le nom du logiciel que vous utilisez : "].lower()
        answer["can be added"] = not is_this_software_exists(
            string)  # don't add an existing software

        # unwanted cases script or too long name
        if len(re.findall("script", string)) > 0:
            answer["can be added"] = False
        if len(string) > MAX_LENGTH:
            answer["can be added"] = False

    return answers


def fill_markdown_template(file_name, answer={}):
    """create the markdown template and fill it with an answer of the form, for the readme.md in the git

    Args:
        file_name (string): the markdown file skeleton, the name of the file in the /static/markdown/ folder
        answer (dict, optional): one answer. Defaults to {}.

    Returns:
        string: filled markdown_skeleton if there was an input answer into the function
    """    

    markdown_skeleton = ""

    with open(markdown_path + file_name, "r") as file:

        sample_file_data = file.read()
        markdown_skeleton = sample_file_data

        if answer:

            software_name = answer["Donnez le nom du logiciel que vous utilisez : "]
            software_description = answer["Expliquez brièvement à quoi sert ce logiciel :"]

            array_software_type = answer['De quel type les sources\xa0du logiciel sont ?'].split(
                ",")  # open source ou non
            # lien code source
            array_code_link = answer['Est-ce que le code source pourrait être distribué\xa0? Si c\'est déjà le cas, vous pouvez nous partager le lien du dépôt avec la réponse "Autre :".'].split(
                ",")
            array_documentation_link = answer['Sur quel support les documentations sont elles disponibles ? Si elle est sur internet et que vous avez le lien, vous pouvez nous le partager avec la réponse "Autre :".'].split(
                ",")  # lien documentation officielle
            array_os = answer['Sur quoi peut-on utiliser le logiciel ? Si vous le savez, quel type de packaging est utilisé ?'].split(
                ",")
            array_difficulty = answer["Comment se passe l'installation du logiciel ?"].split(
                ",")

            for os_name in array_os:  # on affiche que Windows Linux MacOS ou Machine virtuelle
                if os_name == "Windows" or os_name == "Linux" or os_name == "MacOS" or os_name == "Machine virtuelle":
                    pass
                else:
                    array_os.remove(os_name)

            # array_input array_output
            array_input = ""
            try:
                array_input = answer["Quels sont les formats de données de l'on donne au logiciel ? (input)"].split(
                    ",")
            except AttributeError:
                array_input = [" "]

            array_output = ""
            try:
                array_output = answer["Quels formats peut-on récupérer ? (output)"].split(
                    ",")
            except AttributeError:
                array_output = [" "]

            try:
                array_saved_on = answer["Où sont stockées les données traitées\xa0?"].split(
                    ",")
            except AttributeError:
                array_saved_on = [" "]

            """
            invalid_characters = set(string.punctuation)

            if any(char in invalid_characters for char in software_name) or len(software_name) > 30:
                print("Ce logiciel a un nom invalide" +" "+"{ " + software_name + " }")
                software_name = ask_for_valid_string("Le nom ne doit pas contenir de virgule ou est trop long, veuillez entre un nom plus simple")
            """

            markdown_skeleton = re.sub(
                "software_name", software_name, markdown_skeleton, 1)
            # ---
            markdown_skeleton = re.sub(
                "software_description", software_description, markdown_skeleton, 1)
            markdown_skeleton = re.sub("array_software_type", ", ".join(
                array_software_type), markdown_skeleton, 1)
            # ---
            markdown_skeleton = re.sub("array_code_link", str(
                array_code_link[-1]), markdown_skeleton, 1)
            markdown_skeleton = re.sub("array_documentation_link", str(
                array_documentation_link[-1]), markdown_skeleton, 1)
            # ---
            markdown_skeleton = re.sub(
                "array_os", ", ".join(array_os), markdown_skeleton, 1)
            markdown_skeleton = re.sub("array_difficulty", ", ".join(
                array_difficulty), markdown_skeleton, 1)
            # ---
            markdown_skeleton = re.sub("array_input", ", ".join(
                array_input), markdown_skeleton, 1)
            markdown_skeleton = re.sub("array_output", ", ".join(
                array_output), markdown_skeleton, 1)
            markdown_skeleton = re.sub("saved_on", ", ".join(
                array_saved_on), markdown_skeleton, 1)

    return markdown_skeleton

def get_all_beamlines(refined_answers):
    """get all the different beamlines from the list of dicts output from refine_answers_for_softwares_sheets

    Args:
        refined_answers (list of dicts): output from refine_answers_for_softwares_sheets
    """ 

    beamlines = []
    for i in refined_answers:
        beamlines.append(i['beamlines_data'][0].get('beamline name'))

    
    beamlines = list(dict.fromkeys(beamlines)) # remove duplicates
    beamlines.sort() # alphabetically sorted

    return beamlines

