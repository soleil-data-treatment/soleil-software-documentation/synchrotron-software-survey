# Preview du projet

Une version statique du projet est accessible à l'adresse: https://soleil-data-treatment.gitlab.io/soleil-software-documentation/synchrotron-software-survey

# Explication du projet

Le but était de récupérer les réponses d'un Google Form questionnant les scientifiques des lignes de lumières sur l'utilisation de leurs logiciels. On peut afficher les réponses du formulaire de manière organisée, mais aussi créer automatiquement des dépôts Gitlab comprenant de la documentation markdown pour chaque logiciel. Projet MVC fait en Django, Bootstrap, communiquant avec l'API Google et Gitlab.

# Explication des fichiers du projet

Explication de ce qu'il y a dans "myproject": projet de la récupération des données et de leur visualisation via un site web. Projet fait en framework django, python, html, css, javascript.

![Diagramme](diagrams/MVCDiagram.PNG "Diagramme")

## Modèle 
Sur google, base de données sous forme d'un google sheets réunissant les réponses au questionnaire du tour des lignes.

## Vue

- Dossier ```/myproject/static```
    Tous les fichiers qui sont utilisés dans le projet pour lire de données utilisées en interne au projet. Css, javascript etc. Mais aussi des fichiers json pour sauvegarder des données de façon statique, et des tokens (google et gitlab). Une version externe au projet est mise en place avec la commande ```python manage.py collectstatic``` dans le dossier ```/static``` (pas utilisé par les controleurs du projet).

- Fichier ```/myproject/static/json/git-list.json```
    Permet de sauvegarder les logiciels qui sont dans le gitlab.

- Dossier ```/myproject/templates```
    Tous les fichiers html qui servent à la vue dans le model MVC. La base: ```header/body/footer``` sont dans ```/myproject/templates/base```

- Dossier ```/myproject/templatetags```
    ```myfilter.py``` sert à faire des filtres dans la vue: accéder aux données plus facilement dans les fichiers html dans ```/myproject/templates```.

## Controleur

- Fichier ```/myproject/context_processors.py```
    Sert à avoir des variables globales dans le projet: exemple le numéro des logiciels qui peuvent être ajoutés.

- Fichier ```/myproject/forms.py```
    Pour faire des formulaires django.

- Fichier ```/myproject/views.py```
    Python principal pour l'ensemble du projet, les fonctions y sont expliquées dedans. Chacune des fonctions permettent de remplir les vues de façon dynamique.

- Fichier ```/myproject/controllers/auxiliary.py```
    Fonctions auxiliaires pour ```views.py```. Il y a par exemple dedans les fonctions qui permettent d'intéragir avec le google sheets.

- Fichier ```/myproject/controllers/gitlab.py```
    Fonctions pour ```views.py``` qui permettent d'intéragir avec l'API gitlab par le biais d'une librairie python gitlab.

## Django 

- Fichier ```/myproject/settings.py```
    Paramètres django.

- Fichier ```/myproject/urls.py```
    Règles url django.
