"""myproject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from myproject import views

from django.conf.urls import url, include
from django.contrib import admin
 
from django.conf import settings
from django.conf.urls.static import static

from .views import (
    home,
    softwares_sheets,
    all_answers,
    add_new_software,
    login
)
 
urlpatterns = [
    url(r'^$', views.home, name='home'),
    url(r'^softwares_sheets/$', views.softwares_sheets, name='softwares_sheets'),
    url(r'^all_answers/$', views.all_answers, name='all_answers'),
    url(r'^add_new_software/(\d+)?(?:)$', views.add_new_software, name='add_new_software'),
    url(r'^login/$', views.login, name='login'),
    url(r'^admin/', admin.site.urls),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
