from myproject import views

from .views import (
    get_form_answers,
    can_be_added_softwares,
)

# ---- global values in the context for the views

def count_can_be_added_softwares(request):

    answers = get_form_answers()
    answers = can_be_added_softwares(answers) # just add new attribute "can be added" to answers

    count = 0
    for answer in answers:
        if answer["can be added"] == True:
            count+=1

    return {"count_can_be_added_softwares": count}