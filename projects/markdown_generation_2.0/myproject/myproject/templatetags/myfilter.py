from django import template
register = template.Library()

@register.filter(name='getkey')
def getkey(value, arg):
    return value[arg]

@register.filter(name='getindex')
def list_index_from_value(list_, value):
    return list_.index(value)