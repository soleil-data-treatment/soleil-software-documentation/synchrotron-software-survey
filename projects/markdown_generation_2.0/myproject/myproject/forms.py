from django import forms

__all__ = ['NewSoftwareForm', 'UploadDocumentForm', 'UploadDatasetForm']

class NewSoftwareForm(forms.Form):
    software = forms.CharField(required=True, max_length=40)
    software.widget = forms.TextInput(attrs={
        "class":'form-control list-group-item mb-3',
        "id":'software-input', "required":'required',
        "maxlength": '40', 
        "placeholder": "Software's name. No spaces allowed, please check the name online.",
        "aria-describedby": 'software-input-help',
        "onkeypress": 'return event.charCode != 32'
        })
    content = forms.CharField(required=True)
    content.widget = forms.Textarea(attrs={
        "class":'form-control list-group-item',
        "id":'content-input', 
        'required':'required'
        })

CHOICES =( 
    ("1", "One"), 
    ("2", "Two"), 
    ("3", "Three"), 
    ("4", "Four"), 
    ("5", "Five"), 
) 

class UploadDocumentForm(forms.Form):
    Software_for_documentation = forms.ChoiceField(choices = CHOICES, required=True)
    Add_new_documentation = forms.FileField(required=True)

class UploadDatasetForm(forms.Form):
    Software_for_dataset = forms.ChoiceField(choices = CHOICES, required=True)
    Add_new_dataset = forms.FileField(required=True)