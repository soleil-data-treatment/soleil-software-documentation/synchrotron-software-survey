import gitlab
import json

# static path
from myproject.settings import STATIC_PROJECT_ROOT
tokens_path = STATIC_PROJECT_ROOT + "/tokens/"
json_path = STATIC_PROJECT_ROOT + "/json/"
markdown_path = STATIC_PROJECT_ROOT + "/markdown/"

# ---- gitlab functions

def init_gitlabObject():
    """create the gitlab object with the token in /static/tokens/gitlab.json

    Returns:
        gitlab object: gitlab object
    """    

    # key in order to be able to read, write on gitlab
    ACCESS_TOKEN = ''
    with open(tokens_path + 'gitlab.json') as json_token:
        data = json.load(json_token)
        ACCESS_TOKEN = data['ACCESS_TOKEN']
    gl = gitlab.Gitlab("https://gitlab.com", ACCESS_TOKEN)
    return gl


def create_gitlabProject_by_group_name(gitlabObject, project_name, group_name):
    """create a gitlab project with a group name

    Args:
        gitlabObject (gitlab object): gitlab object
        project_name (string): a gitlab project name
        group_name (string): a gitlab group name

    Returns:
        gitlab project object: a gitlab project
    """    

    # You need to get the id of the group, then use the namespace_id attribute
    # to create the group
    group_id = gitlabObject.groups.list(search=group_name)[0].id
    project = gitlabObject.projects.create({
        'name': project_name + " documentation",
        'namespace_id': group_id,
        'visibility': "public",
        'description': "Gathering of documentation, code and tutorials for the " + project_name + " software",
    })
    update_gitlabProject(project)

    return project


def update_gitlabProject(project):
    """update a gitlab project

    Args:
        project (gitlab project object): a gitlab project
    """    

    project.snippets_enabled = 1
    project.save()


def create_gitlabProject_file_stringContent(project, wanted_git_file_path, file_content):
    """create a string content as a file in a gitlab project

    Args:
        project (gitlab project object): a gitlab project
        wanted_git_file_path (string): the wanted location in gitlab for the new file (with the file's name)
        file_content (string): the file's content
    """    

    # See https://docs.gitlab.com/ce/api/commits.html#create-a-commit-with-multiple-files-and-actions
    # for actions detail
    data = {
        'branch': 'master',
        'commit_message': 'Django ' + wanted_git_file_path + ' creation',
        'actions': [
            {
                'action': 'create',
                'file_path': wanted_git_file_path,
                'content': file_content,
            }
        ]
    }

    commit = project.commits.create(data)
    update_gitlabProject(project)

""" 
import requests

def upload_file(project_id, filename, gitlab_token):
    url = 'https://gitlab.com/api/v4/projects/{0}/uploads'.format(project_id)
    headers = {'PRIVATE-TOKEN': gitlab_token}
    files = {'file': open('{0}'.format(filename), 'rb')}
    r = requests.post(url, headers=headers, files=files)

    if r.status_code == 200 or r.status_code == 201:
        print('Uploading the file {0}....'.format(filename))
    else:
        print('File {0} was not uploaded'.format(filename))

    markdown = r.json()['markdown']
    return markdown
"""

import base64
from io import BytesIO

def create_gitlabProject_file_byteContent(project, wanted_git_file_path, file_name, file_content):
    """create a byte content as a file in a gitlab project

    Args:
        project (gitlab project object): a gitlab project
        wanted_git_file_path (string): the wanted location in gitlab for the new file (with the file's name)
        file_name (string): the file's name
        file_content (byte object): the file's content
    """    

    # See https://docs.gitlab.com/ce/api/commits.html#create-a-commit-with-multiple-files-and-actions
    # for actions detail
    # Write the stuff
    try: # file already exists do nothing
        f = open("../cache/" + file_name)
        f.close()
    except IOError:
        with open("../cache/" + file_name, "wb") as f:
            f.write(file_content.getbuffer())   

    data = {
        'branch': 'master',
        'commit_message': 'Django ' + wanted_git_file_path + ' creation',
        'actions': [
            {
                'action': 'create',
                'file_path': wanted_git_file_path,
                'content': base64.b64encode(open("../cache/" + file_name).read()),
                'encoding': 'base64',
            }
        ]
    }

    commit = project.commits.create(data)
    update_gitlabProject(project)
