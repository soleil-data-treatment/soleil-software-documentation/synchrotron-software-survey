# django
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.shortcuts import render
from django.views import generic
from django.template.response import TemplateResponse

# other librairies
import json

# forms.py
from myproject.forms import NewSoftwareForm
from myproject.forms import UploadDocumentForm
from myproject.forms import UploadDatasetForm

# controllers/auxiliary.py functions
from myproject.controllers.auxiliary import get_form_answers
from myproject.controllers.auxiliary import refine_answers_for_softwares_sheets
from myproject.controllers.auxiliary import can_be_added_softwares
from myproject.controllers.auxiliary import fill_markdown_template
from myproject.controllers.auxiliary import get_all_beamlines

# controllers/gitlab.py functions
from myproject.controllers.gitlab import init_gitlabObject
from myproject.controllers.gitlab import create_gitlabProject_by_group_name
from myproject.controllers.gitlab import create_gitlabProject_file_stringContent
from myproject.controllers.gitlab import create_gitlabProject_file_byteContent

# static path
from myproject.settings import STATIC_PROJECT_ROOT
json_path = STATIC_PROJECT_ROOT + "/json/"

# ---- functions for the views

"""
Controllers for the templates, each functions is assigned to a template (template_name: html)
"""

def home(request):
    template_name = "home.html"

    context = {
    }
    return render(request, template_name, context)


def softwares_sheets(request):
    template_name = "softwares_sheets.html"

    answers = get_form_answers()
    refinedAnswers = refine_answers_for_softwares_sheets(
        answers)  # new dictionary, different from answers

    softwares_choices = []
    for counter, i in enumerate(refinedAnswers):
        softwares_choices.append((i["software_infos"]["software name"],
                                  i["software_infos"]["software name"]))  # software's name

    formDocumentation = UploadDocumentForm(request.POST, request.FILES)
    formDataSet = UploadDatasetForm()

    # changement des choix
    formDocumentation.fields["Software_for_documentation"].choices = softwares_choices
    formDataSet.fields["Software_for_dataset"].choices = softwares_choices

    # ---- upload files handler
    if request.method == 'POST':

        Software_for_documentation = request.POST.get(
            'Software_for_documentation', None)
        Add_new_documentation = request.POST.get('Add_new_documentation', None)

        Software_for_dataset_input = request.POST.get(
            'Software_for_dataset', None)
        Add_new_dataset_input = request.POST.get('Add_new_dataset', None)

        # TODO
        # init gitlab object
        gl = init_gitlabObject()
        # search project
        project = gl.projects.list(
            search=Software_for_documentation+" documentation")[0]

        uploaded_file = request.FILES.get("Add_new_documentation")
        create_gitlabProject_file_byteContent(
            project, "/documents/"+uploaded_file.name, uploaded_file.name, uploaded_file.file)

        # documentation
        if Software_for_documentation and Add_new_documentation and formDocumentation.is_valid():
            gl = init_gitlabObject()

        # dataset
        elif Software_for_dataset_input and Add_new_dataset_input and formDataSet.is_valid():
            # init gitlab object
            gl = init_gitlabObject()

    # extra infos v
    allBeamlines = get_all_beamlines(refinedAnswers)

    context = {
        "softwares": refinedAnswers,
        "formDocumentation": formDocumentation,
        "formDataSet": formDataSet,
        # extra infos v
        "softwaresCounter": len(refinedAnswers),
        "beamlinesCounter": len(allBeamlines),
        "percentProgress": len(allBeamlines)/29 * 100,
        "allBeamlines": allBeamlines,
    }
    return render(request, template_name, context)


def all_answers(request):
    template_name = "all_answers.html"

    answers = get_form_answers()
    # just add new attribute "can be added" to answers
    answers = can_be_added_softwares(answers)

    context = {
        "answers": answers
    }
    return render(request, template_name, context)


def add_new_software(request, answer_index=None):
    template_name = "add_new_software.html"

    """
    content handler
    """
    answers = get_form_answers()
    # just add new attribute "can be added" to answers
    answers = can_be_added_softwares(answers)

    if answer_index != None and int(answer_index) and answers[int(answer_index)]["can be added"] == True:
        # if there is an element and can be an int and this software can be added
        initial = {
            'software': answers[int(answer_index)]['Donnez le nom du logiciel que vous utilisez : '],
            'content': fill_markdown_template("sample_fr.md", answers[int(answer_index)])
        }
        form = NewSoftwareForm(initial=initial)

    else:
        initial = {
            'software': '',
            'content': fill_markdown_template("sample_fr.md")
        }
        form = NewSoftwareForm(initial=initial)

    """
    form handler
    """
    if request.method == 'POST':

        # retrieve data
        software_input = request.POST.get('software', None)
        content_input = request.POST.get('content', None)

        if software_input and content_input:  # if form is valid

            # clean data
            project_name = str(software_input).split()[0]
            markdown_content = str(content_input)

            # init gitlab object
            gl = init_gitlabObject()
            # gitlab repo creation
            created_project = create_gitlabProject_by_group_name(
                gl, project_name, "Soleil Software Documentation")
            create_gitlabProject_file_stringContent(
                created_project, "README.md", markdown_content)

            # json file update
            git_list_data = []
            with open(json_path + 'git-list.json', "r") as git_list_json:  # open read only file
                new_software_for_json = {
                    "software name": str(project_name),
                    "git name": str(project_name) + " documentation",
                    "url": "https://gitlab.com/soleil-data-treatment/soleil-software-documentation/" + project_name.lower() + "-documentation"
                }

                git_list_data = json.load(git_list_json)
                git_list_data.append(new_software_for_json)
                git_list_json.close()

            # write changes to the file
            with open(json_path + 'git-list.json', "w+") as git_list_json:
                git_list_json.write(json.dumps(git_list_data, indent=4))
                git_list_json.close()

            # message to show up after the submit
            message = ' "' + str(project_name) + '" has been added.' + \
                " The url is: " + new_software_for_json["url"]
            messages.add_message(request, messages.SUCCESS, message)
        else:
            print(form.errors)

    context = {
        'form': form
    }
    return render(request, template_name, context)


def login(request):
    template_name = "login.html"

    context = {
    }
    return render(request, template_name, context)
