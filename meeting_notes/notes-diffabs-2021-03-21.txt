tour des lignes : DIFFABS
date : 22 mars 2021
================================================================================

RX diffraction diff elastique, absorption
spectr abs. XANES EXAFS, XRF

diff:
  nature des materiaux
  plutot poudres (mono X -> Cristal), desordonnes, textures.
  texture -> etude de qq pics, evolution au cours du temps force, T, P, ..
  
  systeme de detection: ponctuel
  scintillateur ponctuel apres de fentes de detection, position d'ech avant spectr/abs
  chambres a ionisation : spectro abs en transmission
  multi element: spectro abs en fluo, ROI
    fichiers ASCII [ I vs ROI ]
    
  bidim: xpad s140, cirpad
  flyscan: cirpad, Xpad, multi-element. Scan sur monok. Source aimant spectre large.
  50 Hz, 6 Mo/img.
  
  cirpad: apprenti Ing. Developpe outils pour reconstruire images xpad (plat)/cirpad(20 segments polygonal)
  on bouge le det autour de l'echantillon.
  
fluo X: pymca
  contrib: stitching dans PyMCA
  pymca: pb qd spectres trop gros.
scripts internes en plus, pour rassembler des fichiers partiels. A mettre dans PyMCA. Dev par Cristian.
  python 2.7

diff: binoculars, genere volume 3D, peu adapté.
pyfai: utilisation en coordonnees polaires.
  reconstruction en qq millisec. pb a 90 deg.
  cartes de diff. transformation ROI angle->recip -> notebook.


