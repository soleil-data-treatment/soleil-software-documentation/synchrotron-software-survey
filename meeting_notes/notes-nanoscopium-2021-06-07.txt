tour des lignes : NANOSCOPIUM
date : 7 juin 2021
================================================================================

faisceau mono 5-20 keV

STXM faisceau focalisé
====

70 nm avec KB
50 nm avec Fresnel zone-plate

scan echantillon X-Y, vs Energie Fluo. (2 detecteurs a ~180 deg)
detecteur Xia -> NeXus

analyse fluo 500-1000 pixels de coté

faisceau transmis -> bidim comptage de photons Xpad, puis maintenant Merlin et Eiger.
phase en analysant la distribution de faisceau transmis = centre de gravité horz et vert. 

dark field = halo autour du faisceau transmis. interaction avec des grains = SAXS

diffraction WAXS Eiger sur l'axe faisceau type diagramme de poudre.

100-150 nm : distribution elementaire (Fluo X) et phase cristalline (texture et forme de pics de Braggs).

une semaine de manip = 4 To

Faisceau plein champ
====================
plaine champ = tomo faisceau parallèle
detecteur um
temps de mesure 1 minute tomo

faisceau partiellement cohérent =  interferences sur les bords -> paganin

Objectif = workflow
acquisition + traitement par l'utilisateur pour aleger le local contact

tous les controles en Matlab
fichier de config texte
structure metadonnées en .MAT
exportation en TIFF

necessité d'alignement des images du fait du flyscan
temps d'exposition 20 ms/pixel -> dead-time et jitter
recherche de la vitesse constante.
interpolation sur une grille constante et uniforme le long du scan X-Y

cubeXRF = alignement pour chaque canal d'energie fluo a l'aide d'une matrice de transformation
matrice 3D, et 1D en energie

utilisation de pymca et pyfai


pymca: lent sur 1000x1000, pas de correction de position/flyscan
matlab: utilisé a toutes les etapes.

