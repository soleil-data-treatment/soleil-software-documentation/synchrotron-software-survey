Tour des lignes: fait
=====================

- AILES
- ANATOMIX
- ANTARES
- CASSIOPEE
- CRISTAL
- DEIMOS
- DESIRS
- DIFFABS
- DISCO
- GALAXIES
- HERMES
- LUCIA
- MARS
- NANOSCOPIUM
- PSICHE
- PUMA
- PX2
- ROCK
- SAMBA
- SEXTANTS
- SIRIUS
- SIXS
- SMIS
- SWING
- TEMPO

Tour des lignes: en cours/demandé
================================= 
- METROLOGIE  envoyé 9/04/2021
- ODE         pas repondu 03/12/2020
- PLEIADES    envoyé 9/04/2021
- PX1         envoyé 12/02/2021

Message d'invitation
--------------------


Bonjour,

Aurélien Castel a commencé son contrat d'apprentissage dans notre groupe Réduction/Analyse de Données en Septembre dernier. Sa mission initiale est de faire le tour des lignes pour discuter du traitement de données, et des codes que vous utilisez.

Afin de discuter des logiciels et méthodes de traitement de données utilisés sur HERMES nous vous proposons de nous rencontrer. La réunion est ouverte au personnel scientifique de la ligne. Notre groupe sera présent en majorité.

Action : Merci de nous proposer une date pour une réunion en présentiel/visio un lundi ou mardi (Aurélien n'est présent que ces jours la). Envoyer cette invitation aux autres scientifiques de la ligne.

Comment ca se passe:

Vous pouvez nous présenter rapidement (15 minutes) ce que vous faites, et les logiciels que vous utilisez.

On fait le tour de vos habitudes et de vos besoins en traitement de données. Nous remplissons avec vous une fiche (Google form) par logiciel utilisé, avec les points ci-dessous. Nous notons également vos besoins futurs.

Nous vous communiquons le lien vers le formulaire pour que vous puissiez rentrer d'autres informations suite a notre entrevue. Nous vous demandons si besoin une liste d'utilisateurs 'actifs' en traitement de données afin d'étendre le panorama de solutions/besoins.

Une fois que l'on aura vu un nombre suffisant de lignes, les informations anonymes seront mise a disposition et analysées afin d'établir a la fois un ordre de priorité pour notre groupe, et une documentation unifiée et la plus complète possible.

Cela permettra aussi d'identifier les redondances et les priorités d'action.

Informations typiques a discuter, par logiciel/besoin:

·         Logiciels utilisés: présentation, a quoi ca sert, importance pour vous, alternatives.

·         Code source: disponibilité, licence, installation, configuration.

·         Documentation: manuels, tutoriaux, données de test.

·         Données: formats, volumes.


Lien pour répondre au formulaire : https://forms.gle/eaut1STiF2T1XYER7

Notre Gitlab : https://gitlab.com/soleil-data-treatment

 

A bientôt, Emmanuel, pour le groupe.

